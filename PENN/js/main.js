$(document).ready(function () {
	$('.slider').slick({
//		autoplay: true,
		dots: true,
		infinite: true,
		speed: 600,
		slidesToShow: 1,
		adaptiveHeight: true,
		arrows: false,
		appendDots: $('.slider-dots-wrapper')

	});

	$('.slider-prev-but').click(function () {
		$(".slider").slick('slickPrev');
	});

	$('.slider-next-but').click(function () {
		$(".slider").slick('slickNext');
	});

	$(document).ready(function () {
		$(".tabs").lightTabs();
	});

	$('.side-menu .has-ctx').hover(function (e) {
		var ctx = $(this).children('.ctx-menu');
		if (!($('#mobile-indicator').is(':visible'))) {
			ctx.show();
		}

	}, function (e) {
		var ctx = $(this).children('.ctx-menu');
		if (!($('#mobile-indicator').is(':visible'))) {
			ctx.hide();
		}
	});

	$(".cross").hide();
	$(".side-menu .ctx-menu").hide();
	$(".hamburger").click(function () {
		$(".side-menu").slideToggle("slow", function () {
			$(".hamburger").hide();
			$(".cross").show();
		});
	});

	$(".cross").click(function () {
		$(".side-menu").slideToggle("slow", function () {
			$(".cross").hide();
			$(".hamburger").show();
		});
	});

	$('.side-menu > ul > li').click(function (e) {
		var ctx = $(this).children('.ctx-menu');
		if (ctx.is(':visible')) {
			ctx.hide();
		} else {
			ctx.show();
		}
	});

});

(function ($) {
	jQuery.fn.lightTabs = function (options) {

		var createTabs = function () {
			tabs = this;
			i = 0;

			showPage = function (i) {
				$(tabs).children("div").children("div").hide();
				$(tabs).children("div").children("div").eq(i).show();
				$(tabs).children("ul").children("li").removeClass("active");
				$(tabs).children("ul").children("li").eq(i).addClass("active");
			}

			showPage(0);

			$(tabs).children("ul").children("li").each(function (index, element) {
				$(element).attr("data-page", i);
				i++;
			});

			$(tabs).children("ul").children("li").click(function () {
				showPage(parseInt($(this).attr("data-page")));
			});
		};
		return this.each(createTabs);
	};
})(jQuery);
