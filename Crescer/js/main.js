$(document).ready(function () {
	$('.vitrina').slick({
		dots: true,
		infinite: true,
		speed: 500,
		fade: true,
		cssEase: 'linear',
		arrows: false,
		adaptiveHeight: true
	});

	$('.topo').click(function () {
		$('html, body').animate({
			scrollTop: 0
		}, 500);
		return false;
	});


	$('.video').click(function () {
		var video  = $('.video').children("video")[0];
		if (video.paused) {
			video.play();
			$('.video').removeClass('paused');
			$('.slick-dots').css('display', 'none');
		} else {
			video.pause();
			$('.video').addClass('paused');
			$('.slick-dots').css('display', 'block');
		}
	});

	$('.main-menu li').children('.ctx-menu').hide();

	$('.main-menu li').click(function (e) {
		var ctx = $(this).children('.ctx-menu');
		if (ctx.is(':visible')) {
			ctx.hide();
		} else {
			ctx.show();
		}
	});

	$('.main-menu li').hover(function (e) {
		var ctx = $(this).children('.ctx-menu');
		if (!($('#mobile-indicator').is(':visible'))) {
			ctx.show();
		}

	}, function (e) {
		var ctx = $(this).children('.ctx-menu');
		if (!($('#mobile-indicator').is(':visible'))) {
			ctx.hide();
		}
	});

	$(".cross").hide();
	$(".menu").hide();
	$(".hamburger").click(function () {
		$(".top-menu").slideToggle("slow", function () {
			$(".hamburger").hide();
			$(".cross").show();
		});
	});

	$(".cross").click(function () {
		$(".top-menu").slideToggle("slow", function () {
			$(".cross").hide();
			$(".hamburger").show();
		});
	});
});
